﻿#include <iostream>
#include <limits>

//https://docs.microsoft.com/en-us/cpp/c-language/cpp-integer-limits?view=msvc-160

const unsigned char option1 = 0x01; // шестнадцатеричный литерал для 0000 0001
const unsigned char option2 = 0x02; // шестнадцатеричный литерал для 0000 0010
const unsigned char option3 = 0x04; // шестнадцатеричный литерал для 0000 0100
const unsigned char option4 = 0x08; // шестнадцатеричный литерал для 0000 1000
const unsigned char option5 = 0x10; // шестнадцатеричный литерал для 0001 0000
const unsigned char option6 = 0x20; // шестнадцатеричный литерал для 0010 0000
const unsigned char option7 = 0x40; // шестнадцатеричный литерал для 0100 0000
const unsigned char option8 = 0x80; //128 шестнадцатеричный литерал для 1000 0000

using namespace std;

void getTypeOfDigitsSequence(long long number);

void swap(int& a, int& b);


int main1()
{

	int a = 90, b = 89;

	int& r = a;
	r = b;

	//cout << r << endl;
	cout << a << " " << b << endl;
	swap(a, b);
	cout << a << " " << b << endl;

	getTypeOfDigitsSequence(1111111);
	cout << endl;
	getTypeOfDigitsSequence(1234567);
	cout << endl;
	getTypeOfDigitsSequence(-9876542);
	cout << endl;
	getTypeOfDigitsSequence(1234321);
	cout << endl;
	getTypeOfDigitsSequence(112234);
	cout << endl;
	getTypeOfDigitsSequence(77765544);
	cout << endl;
	getTypeOfDigitsSequence(LLONG_MIN);
	cout << endl;

	int number = -127;

	const int size = sizeof(int) * 8;
	char bits[size + 1] = "";
	for (int i = 0; i < size; i++)
	{
		char bit = number & 1 ? '1' : '0';
		number >>= 1;
		bits[size - i - 1] = bit;
	}

	cout << bits << endl;

	return 0;
}

void swap(int& a, int& b)
{
	int t = a;
	a = b;
	b = t;
}

void getTypeOfDigitsSequence(long long number)
{
	const unsigned char LessThan = 1;
	const unsigned char MoreThan = 2;
	const unsigned char Equals = 4;

	unsigned long long temp = 
		number == LLONG_MIN ? 9223372036854775808 : abs(number);

	unsigned char typeOfSequence = 0;

	int previos = temp % 10;
	temp /= 10;
	while (temp != 0)
	{
		int next = temp % 10;
		
		if (previos == next)
		{
			typeOfSequence |= Equals;
		}
		else if (next > previos)
		{
			typeOfSequence |= MoreThan;
		}
		else
		{
			typeOfSequence |= LessThan;
		}

		temp /= 10;
		previos = next;
	}

	switch (typeOfSequence)
	{
	case Equals:
		cout << "Monotonous.";
		break;
	case LessThan:
		cout << "Strictly Increasing.";
		break;
	case MoreThan:
		cout << "Strictly Decreasing.";
		break;
	case Equals | LessThan:
		cout << "Increasing.";
		break;
	case Equals | MoreThan:
		cout << "Decreasing.";
		break;
	default:
		cout << "Unsorted.";
	}
}